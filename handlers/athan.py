from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton,ReplyKeyboardMarkup,KeyboardButton,Location 
from aiogram import types,executor #загрузки библиотеки для работы бота на основе аиограмм
from datetime import timedelta #загрузки библиотеки тоже работа с вренем
from create_bot import dp,bot #Загрузка файла подключения к боту через бот токен
import time,datetime #загрузки библиотеки для работы с временем
import sqlite3 #загрузки библиотеки для работы с базой
import requests #загрузки библиотеки для парсига сайта
import pprint #загрузки библиотеки потом удалить нужен для облегчения моей работы
import config #загрузки конфиг файла
import os #загрузки библиотеки то же что и ппринт
#≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡⇶ Старт основных переменных
# Получения в юникс время сегодняшнего дня
tconv = datetime.datetime.now()
tcon  = int(time.mktime(tconv.timetuple())) 

# Создание уникс эпоха времени для  завтра 
date = tconv + datetime.timedelta(days=1)
lil = date.date()
lol = time.mktime(lil.timetuple())
#≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡⇶ Cоздание таблицы и коннект
conn = sqlite3.connect(config.sqldatabase) #За соединение будет отвечать переменная conn.Если файл уже существует, то функция connect осуществит подключение к нему.Путь должен прописоваться от корня. Узнать можно в настройках
cur = conn.cursor()#После создания объекта соединения с базой данных нужно создать объект cursor
#Теперь выполнять запросы
cur.execute("""CREATE TABLE IF NOT EXISTS actions(
   id INT PRIMARY KEY  ,
   fname TEXT,
   lname TEXT,
   u_id  INT,
   u_uname TEXT,
   u_lang  TEXT,
   latitude TEXT,
   longitude TEXT,
   action TEXT);
   
   """) 
#≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡⇶ Команда /start
async def cmd_hello(message: types.Message):
	#Послать Геопозицю кнопка
    mr =  ReplyKeyboardMarkup(resize_keyboard= True).add(KeyboardButton(text='SEND LOCATION',request_location=True, callback_data='sendG')) 
    # Поздороваться с человеком
    print ({message.from_user.first_name},{message.from_user.last_name},{message.chat.id},{message.chat.username} ) 
    await bot.send_message(message.chat.id, f"{config.hello}({message.chat.title or message.chat.id}){message.from_user.first_name} ! ", reply_markup = mr, parse_mode="HTML") # 
    # Добавим в базу что он нажал на старт
    user = (None,message.from_user.first_name,message.from_user.last_name,message.chat.id,message.chat.username,None,None,None,'Started bot')
    # user = (None, '{message.from_user.first_name}', '{message.from_user.last_name}', '{message.chat.id}','{message.chat.username}','None','None','None','Started bot')
    cur.execute("INSERT INTO actions VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);", user)
    conn.commit()
async def maps(message: types.Message):
    print ("message.text")  

#≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡⇶ Подача Времени молитв на два дня 
async def loc_handler(message: types.Message):
    # Создание сылки для парсинга(сегодняшний день)
    res_url = f'https://api.aladhan.com/v1/timings/{tcon}?latitude={message.location.latitude}&longitude={message.location.longitude}&method=99&methodSettings=17,1,18&tune=0,0,0,4,0,0,1,0,0&school=1&adjustment=1'
    # Создание сылки для парсинга(на завтра) 
    res_url_t = f'https://api.aladhan.com/v1/timings/{lil}?latitude={message.location.latitude}&longitude={message.location.longitude}&method=99&methodSettings=17,1,18&tune=0,0,0,4,0,0,1,0,0&school=1&adjustment=1'
    # Start парсинга на сегодняшний день
    resp = requests.get(res_url, params = {'address': 'London'})
    # Start парсинга на завтра
    resp_t = requests.get(res_url_t, params = {'address': 'London'})
    #Выборка из полученных  данных времена молитв на сегодня .
    # alls = resp.json() 
    # bids = resp.json()['data']['timings']  
    Asr =  resp.json()['data']['timings']['Asr']
    Dhuhr =  resp.json()['data']['timings']['Dhuhr']
    Fajr =  resp.json()['data']['timings']['Fajr']
    Imsak =  resp.json()['data']['timings']['Imsak']
    Isha =  resp.json()['data']['timings']['Isha']
    Maghrib =  resp.json()['data']['timings']['Maghrib']
    Midnight =  resp.json()['data']['timings']['Midnight']
    Sunrise =  resp.json()['data']['timings']['Sunrise']
    Sunset =  resp.json()['data']['timings']['Sunset']
    #Выборка из полученных  данных времена молитв на завтра .
    Y_Asr = resp_t.json()['data']['timings']['Asr']
    Y_Dhuhr = resp_t.json()['data']['timings']['Dhuhr']
    Y_Fajr = resp_t.json()['data']['timings']['Fajr']
    Y_Imsak = resp_t.json()['data']['timings']['Imsak']
    Y_Isha = resp_t.json()['data']['timings']['Isha']
    Y_Maghrib = resp_t.json()['data']['timings']['Maghrib']
    Y_Midnight = resp_t.json()['data']['timings']['Midnight']
    Y_Sunrise = resp_t.json()['data']['timings']['Sunrise']
    Y_Sunset = resp_t.json()['data']['timings']['Sunset']
    

    #составление сообщения для конечной отправки пользователю.
    answ = f"""
<b>Today {tconv.day}-{tconv.month}-{tconv.year}</b>                           
     ⌜-Asr = {Asr}                         
     |-Dhuhr = {Dhuhr}                         
     |-Fajr = {Fajr}                         
     |-Imsak = {Imsak}                         
     |-Isha = {Isha}                         
     |-Maghrib = {Maghrib}                         
     |-Midnight = {Midnight}                         
     |-Sunrise = {Sunrise}                         
     ∟-Sunset = {Sunset}   
<b>Tomorrow {date.day}-{date.month}-{date.year}</b> 
     ⌜-Asr = {Y_Asr}                         
     |-Dhuhr = {Y_Dhuhr}                         
     |-Fajr = {Y_Fajr}                         
     |-Imsak = {Y_Imsak}                         
     |-Isha = {Y_Isha}                         
     |-Maghrib = {Y_Maghrib}                         
     |-Midnight = {Y_Midnight}                         
     |-Sunrise = {Y_Sunrise}                         
     ∟-Sunset = {Y_Sunset}  
      """
    await bot.send_message(message.chat.id,f"Широта:{message.location.longitude}\nДолгота:{message.location.latitude}n {answ}",parse_mode="HTML") # 
    user = (None,message.from_user.first_name,message.from_user.last_name,message.chat.id,message.chat.username,None,message.location.latitude,message.location.longitude,'User requests Athan time')
    cur.execute("INSERT INTO actions VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);", user)
    conn.commit()

    
def register_hendlers_athan(dp: dp):
    dp.register_message_handler(cmd_hello, commands=['start'], commands_prefix='/'  )
    dp.register_message_handler(maps)
    dp.register_message_handler(loc_handler,content_types=['location'])
    